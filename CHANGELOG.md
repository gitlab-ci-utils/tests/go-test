# CHANGELOG

## v1.0.1 (2024-07-22)

### Fixed

- Updated `trigger` pipelines to include `lint_prose` and `code_count` jobs to
  avoid false MR data. If these do not run on all pipelines, any data is
  considered a MR change.

### Miscellaneous

- Fixed issues with package deployment in tag pipelines. Updated release to
  required package deployment.

## v1.0.0 (2024-06-18)

Initial release
