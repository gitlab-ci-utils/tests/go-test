package main

import (
	"log"

	"github.com/thepatrik/strcolor"
)

func main() {
	message := "Hello, World!"
	log.Println(strcolor.Blue(message))
}
